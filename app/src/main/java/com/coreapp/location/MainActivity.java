
    package com.coreapp.location;


    import android.content.Intent;
    import android.support.v7.app.AppCompatActivity;
    import android.os.Bundle;
    import android.view.View;
    import android.widget.Button;

    public class MainActivity extends AppCompatActivity {

        private Button basic;
        private Button customize;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            basic = (Button) findViewById(R.id.basic);
            customize = (Button) findViewById(R.id.customize);
            basic.setOnClickListener(onClickListener);
            customize.setOnClickListener(onClickListener);

        }

        private View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.basic:
                        //DO something
                        Intent intent = new Intent(MainActivity.this, BasicUserLocation.class);
                        startActivity(intent);
                        break;
                    case R.id.customize:
                        //DO something
                        Intent intent1 = new Intent(MainActivity.this, CustomizeUserLocationActivity.class);
                        startActivity(intent1);
                        break;

                }

            }
        };
    }