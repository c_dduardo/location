# Plataforma de aplicativos baseados em localização #

Location é um serviço com o objetivo de implementar as funcionalidades comuns para qualquer aplicativo baseado em localização. Desta forma a camada de negócio será implementada através da location.

### Aplicativo para horários de ônibus###

Pretendemos com esse app mapear os pontos de ônibus e fornecer informações sobre rotas e horários para usuários conectados ou não na internet e que tenham a disposição um smartphone.
Além de utilizá-lo como estudo de caso para as implementações da plataforma Location.

* Inicialmente o app deverá funcionar na cidade de Americana.
* v0.0.1

### Informações sobre configurações do projeto ###

##TODO

### Instruções para contribuidores ###

##TODO

### Fale com os idealizadores ###

##TODO